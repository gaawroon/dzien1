package pl.sda.javalon4.gawronski.teststatic;

public class Main {
    public static void main(String[] args) {
        TestStatic obiekt1 = new TestStatic();
        obiekt1.x = 10;     // to powinno byc setterem

        TestStatic obiekt2 = new TestStatic();
        obiekt2.x = 35;

        System.out.println(obiekt1.x);
        System.out.println(obiekt2.x);

        obiekt1.y = 15;
        obiekt2.y = 18;

        System.out.println(obiekt1.y);
        System.out.println(obiekt2.y);

        obiekt1.y += 1000;
        System.out.println(obiekt1.y);
        System.out.println(obiekt2.y);

        TestStatic.y = 99;
        System.out.println(obiekt1.y);
        System.out.println(obiekt2.y);
        System.out.println(TestStatic.y);       // preferowane

        obiekt1.statycznaMetoda();
        obiekt2.statycznaMetoda();
        TestStatic.statycznaMetoda();           // preferowane

        // -3 => 3
        // 4 => 4
        System.out.println(wlasnyAbs3(-11));
        System.out.println(wlasnyAbs3(8));
    }

    private int wlasnyAbs(int x) {
        if (x < 0) {
            return -1 * x;
        }
        else {
            return x;
        }
    }

    private int wlasnyAbs2(int x) {
        if (x < 0) {
            return -1 * x;
        }
        return x;
    }

    // warunek ? wartosc_jesli_prawda : wartosc_jesli_falsz
    private static int wlasnyAbs3(int x) {
        int y = ( x<0 ? -1*x : x );
        return y;
    }
}
