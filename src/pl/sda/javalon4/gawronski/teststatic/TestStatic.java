package pl.sda.javalon4.gawronski.teststatic;

public class TestStatic {
    int x;
    static int y;

    static void statycznaMetoda() {
        System.out.println("Jestem w statycznej metodzie");
        System.out.println(y);
//         System.out.println(x); (nie mozna wypisac - pole nie jest statycncze)
        statycznaMetoda2();
//        niestatycznaMetoda(); (nie mozna wywolac - metoda nie jest statyczna)
    }

    static void statycznaMetoda2() {
        System.out.println("Jestem w drugiuej statycznej metodzie");
    }

    void niestatycznaMetoda() {
        System.out.println("Jestem w niestatycznej metodzie");
    }

}
