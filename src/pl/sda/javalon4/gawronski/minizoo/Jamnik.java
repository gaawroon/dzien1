package pl.sda.javalon4.gawronski.minizoo;

public class Jamnik extends Dog {
    public int length;

    public Jamnik() {
        System.out.println("Konstruktor z klasy Jamnik");
    }

    public Jamnik(String name) {
        super(name);        // wywoluje konstruktor klasy nadrzednej (w tym przypadku klasy Dog)
        System.out.println("Konstruktor z name z Jamnika");
    }

}
