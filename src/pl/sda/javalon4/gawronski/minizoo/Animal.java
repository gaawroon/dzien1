package pl.sda.javalon4.gawronski.minizoo;

public abstract class Animal {
    public String name;
    public int age;
    public int numberOfLegs;

    public void makeMove() {
        System.out.println("Tup tup tup");
    }

    public abstract void makeNoise();

    public void sayHi() {
        System.out.println("Czesc, nazywam sie " + name);

        System.out.println("Ruch robie tak: ");
        makeMove();

        System.out.println("A odglos robie tak:");
        makeNoise();
    }

    public void takeDrugs() {
        System.out.println("Zwierze o nazwie " + name + " przyjelo leki.");
    }
}
