package pl.sda.javalon4.gawronski.minizoo;

public class Dog extends Animal {

    public Dog() {
        System.out.println("Konstruktor z klasy Dog");
    }

    public Dog(String name) {
        this.name = name;
        System.out.println("Konstruktor z name z Doga");
    }

    @Override
    public void makeNoise() {
        System.out.println("Hau hau");
    }
}
