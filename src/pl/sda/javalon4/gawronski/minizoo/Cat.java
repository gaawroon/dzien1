package pl.sda.javalon4.gawronski.minizoo;

public class Cat extends Animal {

    @Override
    public void makeNoise() {
        System.out.println("Meow meow");
    }
}
