package pl.sda.javalon4.gawronski.minizoo;

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.makeNoise();
        dog.age = 10;
        dog.name = "Reks";
        dog.numberOfLegs = 4;
        dog.makeMove();

        Cat cat = new Cat();
        cat.makeNoise();
        cat.age = 5;
        cat.name = "Bonifacy";
        cat.numberOfLegs = 4;
        cat.makeMove();

        Fish fish = new Fish();
        fish.age = 1;
        fish.name = "Nemo";
        fish.numberOfLegs = 0;
        fish.makeMove();
        fish.makeNoise();

        // Animal animal = new Animal();
        Jamnik jamnik = new Jamnik();
        jamnik.makeNoise();
        jamnik.length = 60;

        System.out.println("******");
        dog.sayHi();
        cat.sayHi();
        fish.sayHi();

        System.out.println("*******");
        Jamnik nowyJamnik = new Jamnik();
        System.out.println("******");
        Jamnik jamnikZName = new Jamnik("Imie jamnika");

        System.out.println("********");
        Weterynarz mojWeterynarz = new Weterynarz();
        mojWeterynarz.lecz(dog);
        mojWeterynarz.lecz(cat);
        mojWeterynarz.lecz(fish);
        mojWeterynarz.lecz(jamnik);
    }
}
