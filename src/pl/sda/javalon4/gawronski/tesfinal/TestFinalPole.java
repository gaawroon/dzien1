package pl.sda.javalon4.gawronski.tesfinal;

public class TestFinalPole {

    // pole final - nie mozna mu zmienic raz zadeklarowanej wartosci (pole jest stala)
    private final int x; // = 10;

//    private static final int y = 20;

    public TestFinalPole(int x) {
        this.x = x;
    }

//    public void incrementX() {
//        x = x + 1;
//    }
//
//    public void setX(int x) {
//        this.x = x;
//    }
}
