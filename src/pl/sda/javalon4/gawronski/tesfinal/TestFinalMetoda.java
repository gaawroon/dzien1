package pl.sda.javalon4.gawronski.tesfinal;

public class TestFinalMetoda {

    // metoda final - nioe mozna jej "Override" w klasach dziedziczacych
    public final void testFinalMetody() {
        System.out.println("Test final metody");
    }
}
