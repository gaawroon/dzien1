package pl.sda.javalon4.gawronski.testinterfejsow;

public class ImplementacjaInterfejsu implements MojPierwszyInterfejs {

    @Override
    public int mojaMetoda(int x, int y) {
        return x+y;
    }
}
