package pl.sda.javalon4.gawronski.testinterfejsow.turnable;

public class Car implements Turnable, Moveable {

    @Override
    public void turnOn() {
        System.out.println("Przekrecam kluczyki");
    }

    @Override
    public void turnOff() {

    }

    @Override
    public void move() {
        System.out.println("Jedziemy kawalek");
    }
}
