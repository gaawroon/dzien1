package pl.sda.javalon4.gawronski.testinterfejsow.turnable;

public class Fan implements Turnable {

    @Override
    public void turnOn() {
        System.out.println("Wkladam kabel do gniazdka");
    }

    @Override
    public void turnOff() {

    }
}
