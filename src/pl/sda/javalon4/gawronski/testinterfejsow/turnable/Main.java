package pl.sda.javalon4.gawronski.testinterfejsow.turnable;

public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        car.turnOn();
        car.move();

        Notebook notebook = new Notebook();
        notebook.turnOn();
        notebook.move();

        Fan fan = new Fan();
        fan.turnOn();
//        fan.move();

        System.out.println(Moveable.x);

        System.out.println("*****");
        Warsztat mojWarsztat = new Warsztat();
        mojWarsztat.napraw(car);
        mojWarsztat.napraw(notebook);
        mojWarsztat.napraw(fan);
        mojWarsztat.napraw(new TV());
    }
}
