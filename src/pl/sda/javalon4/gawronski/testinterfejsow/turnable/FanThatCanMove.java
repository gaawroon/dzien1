package pl.sda.javalon4.gawronski.testinterfejsow.turnable;

public class FanThatCanMove extends Fan implements Moveable {

    @Override
    public void move() {
        System.out.println("Bierzemy wiatrak w inne miejsce");
    }
}
