package pl.sda.javalon4.gawronski.testinterfejsow.turnable;

public class Notebook implements Turnable, Moveable {

    @Override
    public void turnOn() {
        System.out.println("Wciskam guzik wlaczajacy");
    }

    @Override
    public void turnOff() {

    }

    @Override
    public void move() {
        System.out.println("Przenosimy laptopa w inne miejsce");
    }
}
