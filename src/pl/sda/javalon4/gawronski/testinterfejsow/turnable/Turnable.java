package pl.sda.javalon4.gawronski.testinterfejsow.turnable;

public interface Turnable {

    void turnOn();

    void turnOff();

    default void domyslnaImplementacja() {
        System.out.println("To jest domyslna implementacja metody 'domyslnaImplementacja'");
    }

    default void fix() {
        turnOff();
        turnOn();
    }
}
