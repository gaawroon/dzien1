package pl.sda.javalon4.gawronski.testinterfejsow.turnable;

public interface Moveable {

    int x = 10;

    void move();
}
