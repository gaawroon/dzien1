package pl.sda.javalon4.gawronski.testinterfejsow.turnable;

public class TV implements Turnable {
    @Override
    public void turnOn() {
        System.out.println("Wciskam guzik na pilocie");
    }

    @Override
    public void turnOff() {

    }
}
