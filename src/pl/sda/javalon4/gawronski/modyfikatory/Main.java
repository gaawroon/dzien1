package pl.sda.javalon4.gawronski.modyfikatory;

public class Main {
    public static void main(String[] args) {
        Test test = new Test();
        System.out.println(test.stringDefault);
        System.out.println(test.stringProtected);
        System.out.println(test.stringPublic);
    }
}
