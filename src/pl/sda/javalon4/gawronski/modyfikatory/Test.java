package pl.sda.javalon4.gawronski.modyfikatory;

public class Test {
    public String stringPublic;         // - widoczny zewszad
    private String stringPrivate;       // - widoczny tylko z tej klasy
    protected String stringProtected;   // - widoczny tylko z pakietu lub klas dziedziczacych
    String stringDefault;               // - widoczny tylko z pakietu

    private double netPrice = 13.50;

    protected double calculateGrossPrice() {
        return netPrice * 1.23;
    }
}
