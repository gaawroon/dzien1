package pl.sda.javalon4.gawronski.kompozycja;

public class Person {
    private String name;
    private Address address;
    private Person partner;

    public void sayHi() {
        System.out.println("Czesc, nazywam sie " + name + ", mieszkam w " + address.getCity());
        if(partner != null) {
            System.out.println("Moim partnerem/partnerka jest " + partner.getName());
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setPartner(Person partner) {
        this.partner = partner;
    }

    public String getName() {
        return name;
    }
}
