package pl.sda.javalon4.gawronski.kompozycja;

public class Main {
    public static void main(String[] args) {
        Address address1 = new Address("Warszawa", "Al. Ujazdowskie");

        Person person1 = new Person();
        person1.setName("Ania");
        person1.setAddress(address1);

        Person person2 = new Person();
        person2.setName("Marek");
        person2.setAddress(new Address("Warszawa", "Krakowskie Przedmiescie"));

        person2.setPartner(person1);
        person1.setPartner(person2);

        Person person3 = new Person();
        person3.setName("Krystyna");
        person3.setAddress(new Address("Warszawa", "Nowy Swiat"));


        person1.sayHi();
        person2.sayHi();
        person3.sayHi();
    }
}
